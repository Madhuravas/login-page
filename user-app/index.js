const formElement = document.querySelector(".login-container")

formElement.addEventListener('submit', onSubmit)

async function onSubmit(event) {
    event.preventDefault();
    const form = event.target
    let userName = form.querySelector('input.email')
    let password = form.querySelector('input.password')
    let userError = form.querySelector('span.email-err')
    let passwordError = form.querySelector('span.password-err')



    resetField(userError)
    resetField(passwordError)



    if (form) {
        if (userName && userName.value && password && password.value) {
            // let userDataVerify = false
            // let passwordVerify = false
            let newUserData = {
                email: userName.value,
                password: password.value

            }
            // if (userName.value.endsWith("@gmail.com")) {
            //     newUserData.email = userName.value
            //     userDataVerify = true
            // } else {
            //     userError.textContent = "Invalid Email"

            // }

            // if (/[a-zA-Z0-9]+$/.test(password.value) && (password.value.length > 10 && password.value.length < 15)) {
            //     newUserData.password = password.value
            //     passwordVerify = true
            // } else {
            //     passwordError.textContent = "Add valid Password"
            // }

            // if (userDataVerify && userDataVerify) {
            let url = "http://localhost:3000/"
            const response = await fetch(url)
            const data = await response.json()
            console.log(data)
            let isAvl = data.find(({ email, password }) => email === newUserData.email && password === newUserData.password)
            
            if (isAvl) {
                window.location.href = "/home/satish/login_page/user-app/success.html"
            } else {
                passwordError.textContent = "Please Enter valid email and password"
            }
            //}
            form.reset()
        } else {
            if (userName && userName.value) {
                passwordError.textContent = "*Required password"
            } else {
                userError.textContent = "*Required email"
            }
        }
    } else {
        console.log("Invalid Form")
    }



}

let resetField = (field) => {
    field.textContent = ""
}

