let userData = {
    email: "",
    password: ""
}


let signUpFormEl = document.getElementById("signUpForm")

let userEmailEl = document.getElementById("userEmail")
let userPasswordEl = document.getElementById("userPassword")

let emailErrEl = document.getElementById("emailErr")
let passwordErrEl = document.getElementById("passwordErr")

userEmailEl.addEventListener("change", (event) => {
    if (event.target.value.endsWith("@gmail.com")) {
        userData.email = event.target.value
    } else {
        emailErrEl.textContent = "Invalid Email"

    }
    
})

userPasswordEl.addEventListener("change", (event) => {
    if (/[a-zA-Z0-9]+$/.test(event.target.value) && (event.target.value.length > 10 && event.target.value.length < 15)){
        userData.password = event.target.value
    } else {
        passwordErrEl.textContent = "Add valid Password"
    }
    
})


verifyUserData = (userData) => {
    const { email, password } = userData
    if (email === "") {
        emailErrEl.textContent = "Required*"
    }else{
        emailErrEl.textContent = ""
    }
    if (password === "") {
        passwordErrEl.textContent = "Required*"
    }else{
        passwordErrEl.textContent = ""
    }
}


submitFormData = async (userData) => {
    let options = {
        method: "post",
        headers: {
            "Content-Type": "application/json",
            Accept: "application/json"
        },
        body: JSON.stringify(userData)
    }

    let url = "http://localhost:3000/postReq"

    const response = await fetch(url, options)
    
}





signUpFormEl.addEventListener("submit", async (event) => {
    event.preventDefault()
    verifyUserData(userData)
    let url = "http://localhost:3000/"
    const response = await fetch(url)
    const data = await response.json()
    let item = data.find(({ email }) => email === userData.email)
    if (item) {
        emailErrEl.textContent = "email already exist"
    } else {
       if(userData.email === "" || userData.password === ""){
            passwordErrEl.textContent = "Please Enter data"
       }else{
        submitFormData(userData)
        window.location.href = "/home/satish/login_page/user-app/success.html"
       }
    }


    event.target.reset()
})













