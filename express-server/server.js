const express = require("express");
const app = express();
const fs = require("fs")
const cors = require("cors");


app.use(cors())
app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) 

let arrData = fs.readFileSync("/home/satish/login_page/express-server/database.json","utf-8")
arrData = JSON.parse(arrData)

app.get("/",(request,response) =>{
   fs.readFile("database.json","utf-8",(err,data) =>{
     response.send(JSON.parse(data))
   })

})

app.post("/postReq",(request,response) =>{
  const userData = request.body
  arrData.push(userData)
  
  fs.writeFileSync("/home/satish/login_page/express-server/database.json", JSON.stringify(arrData))
})


app.listen(3000, () => {
    console.log("server runs at 3000");
})


